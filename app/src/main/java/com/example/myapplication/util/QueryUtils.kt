package com.example.myapplication.util

import android.text.TextUtils
import android.util.Log

import com.example.myapplication.model.Product
import com.example.myapplication.model.artist

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.nio.charset.Charset
import kotlin.collections.ArrayList

class QueryUtils {
    companion object {
        private val LogTag = this::class.java.simpleName
        private const val BaseURL = "http://ws.audioscrobbler.com/2.0/" // localhost URL

        fun fetchProductData(jsonQueryString: String): ArrayList<Product>? {
            val url: URL? = createUrl("${this.BaseURL}$jsonQueryString")

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
                Log.e(this.LogTag, url.toString())
            }
            catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }

            return extractDataFromJson(jsonResponse)
        }
        fun fetchProductData2(jsonQueryString: String): ArrayList<Product>? {
            val url: URL? = createUrl("${this.BaseURL}$jsonQueryString")

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
                Log.e(this.LogTag, url.toString())
            }
            catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }

            return extractDataFromJson2(jsonResponse)
        }
        fun fetchProductData3(jsonQueryString: String): ArrayList<Product>? {
            val url: URL? = createUrl("${this.BaseURL}$jsonQueryString")

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
                Log.e(this.LogTag, url.toString())
            }
            catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }

            return extractDataFromJson3(jsonResponse)
        }
        fun fetchProductData4(jsonQueryString: String): ArrayList<Product>? {
            val url: URL? = createUrl("${this.BaseURL}$jsonQueryString")

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
                Log.e(this.LogTag, url.toString())
            }
            catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }

            return extractDataFromJson4(jsonResponse)
        }



        private fun createUrl(stringUrl: String): URL? {
            var url: URL? = null
            try {
                url = URL(stringUrl)
            }
            catch (e: MalformedURLException) {
                Log.e(this.LogTag, "Problem building the URL.", e)
            }

            return url
        }

        private fun makeHttpRequest(url: URL?): String {
            var jsonResponse = ""

            if (url == null) {
                return jsonResponse
            }

            var urlConnection: HttpURLConnection? = null
            var inputStream: InputStream? = null
            try {
                urlConnection = url.openConnection() as HttpURLConnection
                urlConnection.readTimeout = 10000 // 10 seconds
                urlConnection.connectTimeout = 15000 // 15 seconds
                urlConnection.requestMethod = "GET"
                urlConnection.connect()

                if (urlConnection.responseCode == 200) {
                    inputStream = urlConnection.inputStream
                    jsonResponse = readFromStream(inputStream)
                }
                else {
                    Log.e(this.LogTag, "Error response code: ${urlConnection.responseCode}")
                }
            }
            catch (e: IOException) {
                Log.e(this.LogTag, "Problem retrieving the product data results: $url", e)
            }
            finally {
                urlConnection?.disconnect()
                inputStream?.close()
            }

            return jsonResponse
        }

        private fun readFromStream(inputStream: InputStream?): String {
            val output = StringBuilder()
            if (inputStream != null) {
                val inputStreamReader = InputStreamReader(inputStream, Charset.forName("UTF-8"))
                val reader = BufferedReader(inputStreamReader)
                var line = reader.readLine()
                while (line != null) {
                    output.append(line)
                    line = reader.readLine()
                }
            }

            return output.toString()
        }

        private fun extractDataFromJson(productJson: String?): ArrayList<Product>? {
            if (TextUtils.isEmpty(productJson)) {
                return null
            }
            Log.e(this.LogTag, productJson)

            val productList = ArrayList<Product>()
            try {
                val baseJsonResponse = JSONObject(productJson).getJSONObject("toptracks").getJSONArray("track")

                //val baseJsonResponse = JSONArray(productJson)
                for (i in 0 until baseJsonResponse.length()) {
                    val productObject = baseJsonResponse.getJSONObject(i)

                    // Images
                    val images = returnValueOrDefault<JSONArray>(productObject, "image") as JSONArray?
                    val imageArrayList = ArrayList<String>()
                    if (images != null) {
                        for (j in 0 until images.length()) {
                            var imagesurl: String=""
                            var imageObj = images.getJSONObject(j)
                            imagesurl = imageObj.getString("#text")
                            Log.e("imageArray",imagesurl)
                            imageArrayList.add(imagesurl)

                        }
                    }

                    val artistDetails = returnValueOrDefault<JSONObject>(productObject, "artist") as JSONObject?
                    var artists:artist=artist()
                    if (artistDetails != null) {
                        artists = artist(
                            returnValueOrDefault<String>(artistDetails, "name") as String,
                            returnValueOrDefault<String>(artistDetails, "mbid") as String,
                            returnValueOrDefault<String>(artistDetails, "url") as String
                        )

//                        artistArrayList.add(returnValueOrDefault<String>(siteDetails, "name") as String)
//                        artistArrayList.add(returnValueOrDefault<String>(siteDetails, "mbid") as String)
//                        artistArrayList.add(returnValueOrDefault<String>(siteDetails, "url") as String)

                        }
                    productList.add(Product(
                        returnValueOrDefault<String>(productObject, "name") as String,
                        returnValueOrDefault<String>(productObject, "duration") as String,
                        returnValueOrDefault<String>(productObject, "playcount") as String,
                        returnValueOrDefault<String>(productObject, "listeners") as String,
                        returnValueOrDefault<String>(productObject, "mbid") as String,
                        returnValueOrDefault<String>(productObject, "url") as String,
                        artists,
                        imageArrayList
                    ))
                }
            }
            catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the product JSON results", e)
            }

            return productList
        }
        private fun extractDataFromJson2(productJson: String?): ArrayList<Product>? {
            if (TextUtils.isEmpty(productJson)) {
                return null
            }
            Log.e(this.LogTag, productJson)

            val productList = ArrayList<Product>()
            try {
                val baseJsonResponse = JSONObject(productJson).getJSONObject("tracks").getJSONArray("track")

                //val baseJsonResponse = JSONArray(productJson)
                for (i in 0 until baseJsonResponse.length()) {
                    val productObject = baseJsonResponse.getJSONObject(i)

                    // Images
                    val images = returnValueOrDefault<JSONArray>(productObject, "image") as JSONArray?
                    val imageArrayList = ArrayList<String>()
                    if (images != null) {
                        for (j in 0 until images.length()) {
                            var imagesurl: String=""
                            var imageObj = images.getJSONObject(j)
                            imagesurl = imageObj.getString("#text")
                            Log.e("imageArray",imagesurl)
                            imageArrayList.add(imagesurl)

                        }
                    }

                    val artistDetails = returnValueOrDefault<JSONObject>(productObject, "artist") as JSONObject?
                    var artists:artist=artist()
                    if (artistDetails != null) {
                        artists = artist(
                            returnValueOrDefault<String>(artistDetails, "name") as String,
                            returnValueOrDefault<String>(artistDetails, "mbid") as String,
                            returnValueOrDefault<String>(artistDetails, "url") as String
                        )

//                        artistArrayList.add(returnValueOrDefault<String>(siteDetails, "name") as String)
//                        artistArrayList.add(returnValueOrDefault<String>(siteDetails, "mbid") as String)
//                        artistArrayList.add(returnValueOrDefault<String>(siteDetails, "url") as String)

                    }
                    productList.add(Product(
                        returnValueOrDefault<String>(productObject, "name") as String,
                        returnValueOrDefault<String>(productObject, "duration") as String,
                        returnValueOrDefault<String>(productObject, "playcount") as String,
                        returnValueOrDefault<String>(productObject, "listeners") as String,
                        returnValueOrDefault<String>(productObject, "mbid") as String,
                        returnValueOrDefault<String>(productObject, "url") as String,
                        artists,
                        imageArrayList
                    ))
                }
            }
            catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the product JSON results", e)
            }

            return productList
        }
        private fun extractDataFromJson3(productJson: String?): ArrayList<Product>? {
            if (TextUtils.isEmpty(productJson)) {
                return null
            }
            Log.e(this.LogTag, productJson)

            val productList = ArrayList<Product>()
            try {
                val baseJsonResponse = JSONObject(productJson).getJSONObject("albums").getJSONArray("album")

                //val baseJsonResponse = JSONArray(productJson)
                for (i in 0 until baseJsonResponse.length()) {
                    val productObject = baseJsonResponse.getJSONObject(i)

                    // Images
                    val images = returnValueOrDefault<JSONArray>(productObject, "image") as JSONArray?
                    val imageArrayList = ArrayList<String>()
                    if (images != null) {
                        for (j in 0 until images.length()) {
                            var imagesurl: String=""
                            var imageObj = images.getJSONObject(j)
                            imagesurl = imageObj.getString("#text")
                            Log.e("imageArray",imagesurl)
                            imageArrayList.add(imagesurl)

                        }
                    }

                    val artistDetails = returnValueOrDefault<JSONObject>(productObject, "artist") as JSONObject?
                    var artists:artist=artist()
                    if (artistDetails != null) {
                        artists = artist(
                            returnValueOrDefault<String>(artistDetails, "name") as String,
                            returnValueOrDefault<String>(artistDetails, "mbid") as String,
                            returnValueOrDefault<String>(artistDetails, "url") as String
                        )

//                        artistArrayList.add(returnValueOrDefault<String>(siteDetails, "name") as String)
//                        artistArrayList.add(returnValueOrDefault<String>(siteDetails, "mbid") as String)
//                        artistArrayList.add(returnValueOrDefault<String>(siteDetails, "url") as String)

                    }
                    productList.add(Product(
                        returnValueOrDefault<String>(productObject, "name") as String,
                        returnValueOrDefault<String>(productObject, "duration") as String,
                        returnValueOrDefault<String>(productObject, "playcount") as String,
                        returnValueOrDefault<String>(productObject, "listeners") as String,
                        returnValueOrDefault<String>(productObject, "mbid") as String,
                        returnValueOrDefault<String>(productObject, "url") as String,
                        artists,
                        imageArrayList
                    ))
                }
            }
            catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the product JSON results", e)
            }

            return productList
        }
        private fun extractDataFromJson4(productJson: String?): ArrayList<Product>? {
            if (TextUtils.isEmpty(productJson)) {
                return null
            }
            Log.e(this.LogTag, productJson)

            val productList = ArrayList<Product>()
            try {
                val baseJsonResponse = JSONObject(productJson).getJSONObject("topartists").getJSONArray("artist")

                //val baseJsonResponse = JSONArray(productJson)
                for (i in 0 until baseJsonResponse.length()) {
                    val productObject = baseJsonResponse.getJSONObject(i)

                    // Images
                    val images = returnValueOrDefault<JSONArray>(productObject, "image") as JSONArray?
                    val imageArrayList = ArrayList<String>()
                    if (images != null) {
                        for (j in 0 until images.length()) {
                            var imagesurl: String=""
                            var imageObj = images.getJSONObject(j)
                            imagesurl = imageObj.getString("#text")
                            Log.e("imageArray",imagesurl)
                            imageArrayList.add(imagesurl)

                        }
                    }

                    val artistDetails = returnValueOrDefault<JSONObject>(productObject, "artist") as JSONObject?
                    var artists:artist=artist()
                    if (artistDetails != null) {
                        artists = artist(
                            returnValueOrDefault<String>(artistDetails, "name") as String,
                            returnValueOrDefault<String>(artistDetails, "mbid") as String,
                            returnValueOrDefault<String>(artistDetails, "url") as String
                        )

//                        artistArrayList.add(returnValueOrDefault<String>(siteDetails, "name") as String)
//                        artistArrayList.add(returnValueOrDefault<String>(siteDetails, "mbid") as String)
//                        artistArrayList.add(returnValueOrDefault<String>(siteDetails, "url") as String)

                    }
                    productList.add(Product(
                        returnValueOrDefault<String>(productObject, "name") as String,
                        returnValueOrDefault<String>(productObject, "duration") as String,
                        returnValueOrDefault<String>(productObject, "playcount") as String,
                        returnValueOrDefault<String>(productObject, "listeners") as String,
                        returnValueOrDefault<String>(productObject, "mbid") as String,
                        returnValueOrDefault<String>(productObject, "url") as String,
                        artists,
                        imageArrayList
                    ))
                }
            }
            catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the product JSON results", e)
            }

            return productList
        }
        private inline fun <reified T> returnValueOrDefault(json: JSONObject, key: String): Any? {
            when (T::class) {
                String::class -> {
                    return if (json.has(key)) {
                        json.getString(key)
                    } else {
                        ""
                    }
                }
                Int::class -> {
                    return if (json.has(key)) {
                        json.getInt(key)
                    }
                    else {
                        return -1
                    }
                }
                Double::class -> {
                    return if (json.has(key)) {
                        json.getDouble(key)
                    }
                    else {
                        return -1.0
                    }
                }
                Long::class -> {
                    return if (json.has(key)) {
                        json.getLong(key)
                    }
                    else {
                        return (-1).toLong()
                    }
                }
                JSONObject::class -> {
                    return if (json.has(key)) {
                        json.getJSONObject(key)
                    }
                    else {
                        return null
                    }
                }
                JSONArray::class -> {
                    return if (json.has(key)) {
                        json.getJSONArray(key)
                    }
                    else {
                        return null
                    }
                }
                else -> {
                    return null
                }
            }
        }
    }
}