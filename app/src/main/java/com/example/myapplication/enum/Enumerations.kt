package com.example.myapplication.enum

enum class UserInterfaceState {
    NETWORK_ERROR,
    NO_DATA,
    RESET,
    HOME,
    RESULTS
}