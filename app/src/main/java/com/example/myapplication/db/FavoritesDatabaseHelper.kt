package com.example.myapplication.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

class FavoritesDatabaseHelper(context: Context): SQLiteOpenHelper(context, DbSettings.DB_NAME, null, DbSettings.DB_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        val createFavoritesTableQuery = "CREATE TABLE " + DbSettings.DBFavoriteEntry.TABLE + " ( " +
                DbSettings.DBFavoriteEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +

                DbSettings.DBFavoriteEntry.COL_FAV_NAME + " TEXT NULL, " +
                DbSettings.DBFavoriteEntry.COL_DURATION + " TEXT NULL, " +
                DbSettings.DBFavoriteEntry.COL_PLAYCOUNT + " TEXT NULL, " +
                DbSettings.DBFavoriteEntry.COL_LISTENERS + " TEXT NULL, " +
                DbSettings.DBFavoriteEntry.COL_MBID + " TEXT NULL, " +
                DbSettings.DBFavoriteEntry.COL_URL + " TEXT NULL);"

        val createImageAssetTableQuery = "CREATE TABLE " + DbSettings.DBImageAssetEntry.TABLE + " ( " +
                DbSettings.DBImageAssetEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBImageAssetEntry.FAVORITE_ID + " INTEGER NOT NULL, " +
                DbSettings.DBImageAssetEntry.COL_IMAGE + " TEXT NULL, " +
                "CONSTRAINT fk_favorites FOREIGN KEY(" + DbSettings.DBImageAssetEntry.FAVORITE_ID + ") " +
                "REFERENCES " + DbSettings.DBFavoriteEntry.TABLE + "(" + DbSettings.DBFavoriteEntry.ID + ") ON DELETE CASCADE);"

        val createArtistTableQuery = "CREATE TABLE " + DbSettings.DBArtistEntry.TABLE + " ( " +
                DbSettings.DBArtistEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBArtistEntry.FAVORITE_ID + " INTEGER NOT NULL, " +
                DbSettings.DBArtistEntry.COL_MBID+" TEXT NULL, "+
                DbSettings.DBArtistEntry.COL_NAME + " TEXT NULL, " +
                DbSettings.DBArtistEntry.COL_URL + " TEXT NULL, " +
                " FOREIGN KEY(" + DbSettings.DBArtistEntry.FAVORITE_ID + ") " +
                "REFERENCES " + DbSettings.DBFavoriteEntry.TABLE + "(" + DbSettings.DBFavoriteEntry.ID + ") ON DELETE CASCADE);"


        db?.execSQL(createFavoritesTableQuery)
        Log.e("database", "success create table favorite")
        db?.execSQL(createImageAssetTableQuery)
        Log.e("database", "success create table Image")
        db?.execSQL(createArtistTableQuery)
        Log.e("database", "success create table artist")


    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS " + DbSettings.DBFavoriteEntry.TABLE)
        onCreate(db)
    }
}