package com.example.myapplication

import android.app.ActionBar
import android.app.Dialog
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.widget.Button
import com.example.myapplication.R
import com.example.myapplication.fragment.FavoritesFragment
import com.example.myapplication.fragment.HomeFragment
import com.example.myapplication.fragment.NoConnectionFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private var isNetworkConnected = false

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.e("FAVORITE", "Added favorite but came back here...")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        nav_view.setCheckedItem(R.id.nav_home)

        // Load Fragment into View
        val fm = supportFragmentManager

        // add
        val ft = fm.beginTransaction()
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        if (networkInfo == null) {
            Log.e("NETWORK", "not connected")
            ft.add(R.id.frag_placeholder, NoConnectionFragment())
        }
        else {
            Log.e("NETWORK", "connected")

            ft.add(R.id.frag_placeholder, HomeFragment(this,0), "HOME_FRAG")
            this.isNetworkConnected = true
        }
        ft.commit()

        supportActionBar?.title = "Home"
        supportActionBar?.subtitle = "What's New"
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                if (this.isNetworkConnected) {
                    // Load Fragment into View
                    val fm = supportFragmentManager

                    // add
                    val ft = fm.beginTransaction()
                    ft.replace(R.id.frag_placeholder, HomeFragment(this@MainActivity,0), "HOME_FRAG")
                    ft.commit()

                    supportActionBar?.title = "Home"
                    supportActionBar?.subtitle = "top tracks"
                }
            }
            R.id.nav_favorites -> {
                // Load Fragment into View
                val fm = supportFragmentManager

                // add
                val ft = fm.beginTransaction()
                ft.remove(fm.findFragmentById(R.id.frag_placeholder)!!)
                ft.add(R.id.frag_placeholder, FavoritesFragment(this@MainActivity), "FAVORITES_FRAG")
                ft.commit()

                supportActionBar?.title = "Favorites"
                supportActionBar?.subtitle = ""
            }
            R.id.nav_top_albums -> {
                // Load Fragment into View
                val fm = supportFragmentManager

                // add
                val ft = fm.beginTransaction()
                ft.remove(fm.findFragmentById(R.id.frag_placeholder)!!)
                ft.add(R.id.frag_placeholder, HomeFragment(this@MainActivity,1), "HOME_FRAG")
                ft.commit()

                supportActionBar?.title = "Home"
                supportActionBar?.subtitle = "top albums"
            }
            R.id.nav_top_artist -> {
            // Load Fragment into View
            val fm = supportFragmentManager

            // add
            val ft = fm.beginTransaction()
            ft.remove(fm.findFragmentById(R.id.frag_placeholder)!!)
            ft.add(R.id.frag_placeholder, HomeFragment(this@MainActivity,2), "HOME_FRAG")
            ft.commit()

            supportActionBar?.title = "Home"
            supportActionBar?.subtitle = "top artists"
        }

        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }


}

