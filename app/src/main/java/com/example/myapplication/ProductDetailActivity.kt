package com.example.myapplication

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.TextView
import android.widget.Toast
import com.example.myapplication.R
import com.example.myapplication.model.Product
import com.example.myapplication.viewModel.ProductViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_product_detail.*
import kotlinx.android.synthetic.main.feature_list_item.view.*

class ProductDetailActivity: AppCompatActivity() {
    private lateinit var product: Product
    private lateinit var viewModel: ProductViewModel



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)

        product = intent.extras!!.getSerializable("PRODUCT") as Product
        Log.e("FAVORITE", product.isFavorite.toString())

        viewModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)

        this.loadUI(product)
    }
    override fun onBackPressed() {
        this.finish()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.product_detail_menu, menu)
        if (this.product.isFavorite) {
            menu?.getItem(0)?.icon = getDrawable(R.drawable.ic_star_24dp)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                this.finish()
                return true
            }
            R.id.action_favorite -> {
                // TODO: Implement according to the given pseudocode
                if (this.product.isFavorite) {
                    item.icon = getDrawable(R.drawable.ic_star_border_24dp)
                    viewModel.removeFavorite(this.product.getTName())
                }
                else {
                    item.icon = getDrawable(R.drawable.ic_star_24dp)
                    viewModel.addFavorite(this.product)
                }
                this.product.isFavorite = !this.product.isFavorite
                return false

            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun loadUI(product: Product) {
        product_title.text = product.getTName()
        manufacturer.text = product.getPlaycount()
        brand.text = product.getListener()
        model.text = product.getUrl()


        val images = product.getImage()
        if (images.size > 0) {
            Picasso.with(this).load(product.getImage()[3]).into(product_img)
        }
        else {
            // eventually show image not available pic
        }

    }


}