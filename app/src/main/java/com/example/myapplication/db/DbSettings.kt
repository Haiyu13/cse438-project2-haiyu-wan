package com.example.myapplication.db

import android.provider.BaseColumns

class DbSettings {
    companion object {
        const val DB_NAME = "favourite.db"
        const val DB_VERSION = 3
    }

    class DBFavoriteEntry: BaseColumns {
        companion object {
            const val TABLE = "favorites"
            const val ID = BaseColumns._ID
            const val COL_FAV_NAME = "trackname"
            const val COL_DURATION = "duration"
            const val COL_PLAYCOUNT = "playcount"
            const val COL_LISTENERS = "listeners"
            const val COL_MBID = "trackmbid"
            const val COL_URL = "trackurl"
        }
    }

    class DBImageAssetEntry: BaseColumns {
        companion object {
            const val TABLE = "image"
            const val ID = BaseColumns._ID
            const val FAVORITE_ID = "favorite_id"
            const val COL_IMAGE = "image"
        }
    }

    class DBArtistEntry: BaseColumns {
        companion object {
            const val TABLE = "artists"
            const val ID = BaseColumns._ID
            const val FAVORITE_ID = "favorite_id"
            const val COL_NAME = "artistname"
            const val COL_MBID = "artistmbid"
            const val COL_URL="artisturl"
        }
    }


}