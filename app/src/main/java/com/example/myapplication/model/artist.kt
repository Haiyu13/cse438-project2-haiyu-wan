package com.example.myapplication.model

import android.view.View
import java.io.Serializable
import kotlin.collections.ArrayList

class artist(): Serializable {
    private var name:String =""


    private var mbid: String =""
    private var url: String =""

    var isFavorite: Boolean = false


    constructor(
        name:String,

        mbid: String,
        url: String

    ):this(){
        this.name=name
        this.mbid=mbid
        this.url=url


    }

    fun getTName(): String {
        return this.name
    }

    fun getMbid(): String {
        return this.mbid
    }
    fun getUrl(): String {
        return this.url
    }

    fun getName(): String {
        return this.name
    }
}


