package com.example.myapplication.model

import android.view.View
import java.io.Serializable
import kotlin.collections.ArrayList

class Product(): Serializable {
    private var name:String =""
    private var duration: String =""
    private var playcount: String =""
    private var listener: String =""
    private var mbid: String =""
    private var url: String =""
    private var artist: artist=artist()
    private var image: ArrayList<String> = ArrayList()
    var isFavorite: Boolean = false


    constructor(
        name:String,
        duration: String,
        playcount: String,
        listener: String,
        mbid: String,
        url: String,
        artist: artist,
        image: ArrayList<String>
    ):this(){
        this.name=name
        this.duration=duration
        this.playcount=playcount
        this.listener=listener
        this.mbid=mbid
        this.url=url
        this.artist=artist
        this.image=image

    }

    fun getTName(): String {
        return this.name
    }
    fun getDuration(): String {
        return this.duration
    }
    fun getPlaycount(): String {
        return this.playcount
    }
    fun getListener(): String {
        return this.listener
    }
    fun getMbid(): String {
        return this.mbid
    }
    fun getUrl(): String {
        return this.url
    }
    fun getArtist(): artist {
        return this.artist
    }
    fun getImage(): ArrayList<String>  {
        return this.image
    }
    fun getName(): String {
        return this.name
    }
}


