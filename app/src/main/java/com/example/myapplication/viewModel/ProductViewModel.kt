package com.example.myapplication.viewModel

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import android.os.AsyncTask
import android.provider.MediaStore
import android.util.Log
import com.example.myapplication.db.DbSettings
import com.example.myapplication.db.FavoritesDatabaseHelper
import com.example.myapplication.model.artist
import com.example.myapplication.model.Product

import com.example.myapplication.util.QueryUtils

class ProductViewModel(application: Application): AndroidViewModel(application) {
    private var _favouriteDBHelper: FavoritesDatabaseHelper = FavoritesDatabaseHelper(application)
    private var _productsList: MutableLiveData<ArrayList<Product>> = MutableLiveData()
    private var is_new:Int=0;
    fun getNewProducts(): MutableLiveData<ArrayList<Product>> {
        is_new = 1;
        loadProducts("?method=tag.gettoptracks&tag=top+track&api_key=cd454f1b18bfda462aa5af23d282ba11&format=json")
        return _productsList
    }
    fun getNewProducts2(): MutableLiveData<ArrayList<Product>> {
        is_new = 2;
        loadProducts("?method=tag.gettopalbums&tag=disco&api_key=cd454f1b18bfda462aa5af23d282ba11&format=json")
        return _productsList
    }
    fun getNewProducts3(): MutableLiveData<ArrayList<Product>> {
        is_new = 3;
        loadProducts("?method=tag.gettopartists&tag=disco&api_key=cd454f1b18bfda462aa5af23d282ba11&format=json")
        return _productsList
    }

    fun getProductsByQueryText(query: String): MutableLiveData<ArrayList<Product>> {
        loadProducts("?method=artist.gettoptracks&artist=$query&api_key=cd454f1b18bfda462aa5af23d282ba11&format=json")
        return _productsList
    }

    private fun loadProducts(query: String) {
        ProductAsyncTask().execute(query)
    }

    @SuppressLint("StaticFieldLeak")
    inner class ProductAsyncTask: AsyncTask<String, Unit, ArrayList<Product>>() {
        override fun doInBackground(vararg params: String?): ArrayList<Product>? {
            if(is_new==1){
                return QueryUtils.fetchProductData2(params[0]!!)
            }
            else if(is_new==2){
                return QueryUtils.fetchProductData3(params[0]!!)
            }
            else if(is_new==3){
                return QueryUtils.fetchProductData4(params[0]!!)
            }

            return QueryUtils.fetchProductData(params[0]!!)
        }

        override fun onPostExecute(result: ArrayList<Product>?) {
            if (result == null) {
                Log.e("RESULTS", "No Results Found")
            }
            else {
                Log.e("RESULTS", result.toString())
                // Tag Items with favorites
                val favorites = this@ProductViewModel.loadFavorites()
                val resultList = ArrayList<Product>()
                for (item in result) {
                    for (fav in favorites) {
                        if (fav.getTName() == item.getTName()) {
                            item.isFavorite = true
                        }
                    }
                    resultList.add(item)
                }

                _productsList.value = resultList
            }
        }
    }

    fun getFavorites(): MutableLiveData<ArrayList<Product>> {
        val returnList = this.loadFavorites()
        this._productsList.value = returnList
        return this._productsList
    }

    private fun loadFavorites(): ArrayList<Product> {
        val favorites: ArrayList<Product> = ArrayList()
        val database = this._favouriteDBHelper.readableDatabase
        Log.e("load", "oncreate called")
        val projection = arrayOf(DbSettings.DBFavoriteEntry.COL_FAV_NAME,DbSettings.DBFavoriteEntry.COL_DURATION,DbSettings.DBFavoriteEntry.COL_LISTENERS,DbSettings.DBFavoriteEntry.COL_MBID,DbSettings.DBFavoriteEntry.COL_URL,DbSettings.DBFavoriteEntry.ID,DbSettings.DBFavoriteEntry.COL_PLAYCOUNT)
        val cursor = database.query(
            DbSettings.DBFavoriteEntry.TABLE,
            projection,
            null, null, null, null, DbSettings.DBFavoriteEntry.COL_FAV_NAME
        )

        while (cursor.moveToNext()) {
            val cursorId = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.ID)
            val cursorName = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_FAV_NAME)
            val cursorDuration = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_DURATION)
            val cursorPlaycount = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_PLAYCOUNT)
            val cursorListeners = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_LISTENERS)
            val cursorMbid = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_MBID)
            val cursorUrl = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_URL)


            // TODO: Load up a Cursor object for images similar to how the gtinCursor is loaded
            val imgCursor = database.query(
                DbSettings.DBImageAssetEntry.TABLE,
                arrayOf(
                    DbSettings.DBImageAssetEntry.COL_IMAGE
                ),
                "${DbSettings.DBImageAssetEntry.FAVORITE_ID}=?", arrayOf(cursor.getLong(cursorId).toString()), null, null, null
            )
            val images = ArrayList<String>()
            while (imgCursor.moveToNext()) {
                images.add(imgCursor.getString(imgCursor.getColumnIndex(DbSettings.DBImageAssetEntry.COL_IMAGE)))
            }
            imgCursor.close()
            // TODO: Iterate over the Cursor object above and add the contents to the images ArrayList above; Finally, close the Cursor
            Log.e("load","images fine")

            val artistCursor = database.query(
                DbSettings.DBArtistEntry.TABLE,
                arrayOf(
                    DbSettings.DBArtistEntry.COL_NAME,
                    DbSettings.DBArtistEntry.COL_MBID,
                    DbSettings.DBArtistEntry.COL_URL
                ),
                "${DbSettings.DBArtistEntry.FAVORITE_ID}=?", arrayOf(cursor.getLong(cursorId).toString()), null, null, null
            )
            var artists :artist = artist()
            while (artistCursor.moveToNext()) {
                val detailCursorId = artistCursor.getColumnIndex(DbSettings.DBArtistEntry.ID)
                if (detailCursorId != -1) {
                   artists= artist((cursor.getString(cursor.getColumnIndex(DbSettings.DBArtistEntry.COL_NAME)) ),
                    (cursor.getString(cursor.getColumnIndex(DbSettings.DBArtistEntry.COL_MBID))),
                    (cursor.getString(cursor.getColumnIndex(DbSettings.DBArtistEntry.COL_URL))))

                }
            }
            artistCursor.close()



            val product = Product(
                cursor.getString(cursorName),
                cursor.getString(cursorDuration),
                cursor.getString(cursorPlaycount),
                cursor.getString(cursorListeners),
                cursor.getString(cursorMbid),
                cursor.getString(cursorUrl),
                artists,
                images
            )
            product.isFavorite = true
            favorites.add(product)
        }

        cursor.close()
        database.close()

        return favorites
    }

    fun addFavorite(product: Product) {
        val database: SQLiteDatabase = this._favouriteDBHelper.writableDatabase

        val favValues = ContentValues()
        favValues.put(DbSettings.DBFavoriteEntry.COL_FAV_NAME, product.getTName())
        favValues.put(DbSettings.DBFavoriteEntry.COL_DURATION, product.getDuration())
        favValues.put(DbSettings.DBFavoriteEntry.COL_PLAYCOUNT, product.getPlaycount())
        favValues.put(DbSettings.DBFavoriteEntry.COL_LISTENERS, product.getListener())
        favValues.put(DbSettings.DBFavoriteEntry.COL_MBID, product.getMbid())
        favValues.put(DbSettings.DBFavoriteEntry.COL_URL, product.getUrl())

        val favId = database.insertWithOnConflict(
            DbSettings.DBFavoriteEntry.TABLE,
            null,
            favValues,
            SQLiteDatabase.CONFLICT_REPLACE
        )

        // TODO: Loop over the product's sem3 Images and add them to the images table similarly to how the gtins are added below
        for (img in product.getImage()) {
            val imgValues = ContentValues()
            imgValues.put(DbSettings.DBImageAssetEntry.FAVORITE_ID, favId)
            imgValues.put(DbSettings.DBImageAssetEntry.COL_IMAGE, img)
            database.insertWithOnConflict(
                DbSettings.DBImageAssetEntry.TABLE,
                null,
                imgValues,
                SQLiteDatabase.CONFLICT_REPLACE
            )
        }
        val detail = product.getArtist()
        val detailValues = ContentValues()
        detailValues.put(DbSettings.DBArtistEntry.FAVORITE_ID, favId)
        detailValues.put(DbSettings.DBArtistEntry.COL_NAME, detail.getName())
        detailValues.put(DbSettings.DBArtistEntry.COL_MBID, detail.getMbid())
            detailValues.put(DbSettings.DBArtistEntry.COL_URL, detail.getUrl())
            val detailId = database.insertWithOnConflict(
                DbSettings.DBArtistEntry.TABLE,
                null,
                detailValues,
                SQLiteDatabase.CONFLICT_REPLACE
                )

//        for (detail in product.getArtist()) {
//            val detailValues = ContentValues()
//            detailValues.put(DbSettings.DBArtistEntry.FAVORITE_ID, favId)
//            detailValues.put(DbSettings.DBArtistEntry.COL_NAME, detail.getName())
//            detailValues.put(DbSettings.DBArtistEntry.COL_MBID, detail.getMbid())
//            detailValues.put(DbSettings.DBArtistEntry.COL_URL, detail.getUrl())
//            val detailId = database.insertWithOnConflict(
//                DbSettings.DBArtistEntry.TABLE,
//                null,
//                detailValues,
//                SQLiteDatabase.CONFLICT_REPLACE
//            )
//
//        }

        database.close()
    }

    fun removeFavorite(name: String, isFromResultList: Boolean = false) {
        val database = _favouriteDBHelper.writableDatabase

        database.delete(
            DbSettings.DBFavoriteEntry.TABLE,
            "${DbSettings.DBFavoriteEntry.COL_FAV_NAME}=?",

            arrayOf(name)
        )
        database.close()

        var index = 0
        val favorites = this._productsList.value
        if (favorites != null) {
            for (i in 0 until favorites.size) {
                if (favorites[i].getTName() == name) {
                    index = i
                }
            }
            if (isFromResultList) {
                favorites[index].isFavorite = false
            }
            else {
                favorites.removeAt(index)
            }

            this._productsList.value = favorites
        }
    }
}